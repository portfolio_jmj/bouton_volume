
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
const vAudio = new AudioContext();
console.log("vAudio", vAudio.destination);

const vGain = vAudio.createGain();
vGain.gain.value = 0;

const audioElement = document.getElementById("audio");
const track = vAudio.createMediaElementSource(audioElement);
track.connect(vGain).connect(vAudio.destination);
audioElement.onplay = () => {
    vAudio.resume()
}

const targetNode = document.getElementById("external");
let centerX = targetNode.offsetLeft + targetNode.offsetWidth / 2;
let centerY = targetNode.offsetTop + targetNode.offsetHeight / 2;
console.log(centerX,centerY);

var rect = targetNode.getBoundingClientRect();
console.log("top",rect.top)
const a = new Point(centerX, centerY);
const b = new Point(centerX,rect.top)

const ab = calculDistance(b);
console.log("ab",ab)
function HandleOnChange(value) {
    console.log(value);
    soundVolume = value / 100;
    vGain.gain.value = soundVolume;
    const valueDeg = Math.round(value * 3.6);
    document.getElementById("external").style = `--deg:${-valueDeg}deg`;
    document.getElementById("inside").style = `--deg:${valueDeg}deg`;
    document.getElementById("value").innerText = value;
}

function HandleOnClickExternla(e) {
    const c = new Point(e.x,e.y)
    console.log("e",e)
    console.log("c",c)
    const ac = calculDistance(c);
    console.log("ac",ac)
    const angle = calculAngle(ab,ac);
    console.log("angle",angle);
    vGain.gain.value = Math.round(angle/3.6/100);
}

/**
 * 
 * @param {Point} p2 
 * @returns {number}
 */
function calculDistance(p2,p1){
    const x = Math.pow((p2.x - a.x),2);
    const y = Math.pow((p2.y - a.y),2);
    return Math.sqrt(x+y);
}

function calculAngle(ab,ac){
    const cosValue = ac/ab;
    const x = Math.acos(cosValue);
    let degrees = x * (180 / Math.PI);
    return degrees;
}